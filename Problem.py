
class Problem:
    def __init__(self, file):
        self.file = file
        print("Hola")

    def getInitialData(self):
        print("Leo los datos del problema")

    def createProblemStructure(self):
        print("Asigno los datos a la estructura de datos")

    def solve(self,algorithm):
        print("Resuelvo el problema, puedo generar distintas instacias y que este metodo ejecute algoritmos diferentes")
        algorithm.exec()

    def printResults(self):
        print("Genero el fichero con los resultado para su evaluacion")

    def run(self,algorithm):
        self.getInitialData()
        self.createProblemStructure()
        self.solve(algorithm)
        self.printResults()
